package com.cmu.fabrice.locateme;


/**
 * This class in the main activity of an Android application which get the location of the phone holder,
 * displays it on the map and is capable of sending an SMS.
 *
 * @author: Fabrice Kwizera
 *
 */


import android.Manifest;
import android.app.Activity;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.telephony.SmsManager;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;

public class MainActivity extends AppCompatActivity {

    private static final int LOCATION = 123;
    private static final int SMS = 456;
    private LocationManager locationManager;
    private double longitude = 0;
    private double latitude = 0;
    private Button locateBtn;
    private Button sendSMSBtn;
    private TextView latitudeView;
    private TextView longitudeView;
    private EditText phoneNumber;
    private MapFragment fragment = new MapFragment();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        locateBtn = findViewById(R.id.mapbtn);
        sendSMSBtn = findViewById(R.id.smsbtn);
        latitudeView = findViewById(R.id.latitudeText);
        longitudeView = findViewById(R.id.longitudeText);
        phoneNumber = findViewById(R.id.phonetxt);

        /** Below OnClickListener executes the code in the onClick () when the LOCATE ME button is clicked */
        locateBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getLocation();
            }
        });

        /** Below onClickListener sends an SMS when the SEND SMS button is clicked */
        sendSMSBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendSMS();
            }
        });

    }

    /**
     * Below method plots the map on the layout after it instantiates the fragment
     * and uses fragmentTransaction to send it to the map holder defined in activity main
     *  */
    public void plotTheMap(){
        fragment.setLongitude(longitude);
        fragment.setLatitude(latitude);
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.mapHolder, fragment);
        fragmentTransaction.commit();
    }


    /** Below methods gets the location of the user after asking for permission */
    protected void getLocation() {
        if (ContextCompat.checkSelfPermission(MainActivity.this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            /** Permission refused */
            if (ActivityCompat.shouldShowRequestPermissionRationale(MainActivity.this, Manifest.permission.ACCESS_FINE_LOCATION)) {
            } else {
                ActivityCompat.requestPermissions(MainActivity.this,
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        LOCATION);
            }
        } else {
            /** Permission to get location has been granted */
            FusedLocationProviderClient client = LocationServices.getFusedLocationProviderClient(this);
            Location location = client.getLastLocation().getResult();
            longitude = location.getLongitude();
            longitudeView.setText("Long: " + longitude + "");
            fragment.setLongitude(longitude);
            latitude = location.getLatitude();
            latitudeView.setText("Lat: "+ latitude + "");
            fragment.setLatitude(latitude);
            plotTheMap();
        }
    }

    /** Below methods gets sends an SMS after asking for permission */
    public void sendSMS() {
        if (ContextCompat.checkSelfPermission(MainActivity.this, Manifest.permission.SEND_SMS) != PackageManager.PERMISSION_GRANTED) {
            /** Permission refused */
            if (ActivityCompat.shouldShowRequestPermissionRationale(MainActivity.this,
                    Manifest.permission.SEND_SMS)) {
            } else {
                ActivityCompat.requestPermissions(MainActivity.this,
                        new String[]{Manifest.permission.SEND_SMS},
                        SMS);
            }
        } else {
            /** Permission to send SMS granted */
            getLocation();
            String message = "This device is located at " + latitude + " latitude and " + longitude + " longitude.";
            String number = phoneNumber.getText().toString();
            String sent = "SMS_SENT";
            String delivered = "SMS delivered";
            SmsManager smsManager = SmsManager.getDefault();
            PendingIntent sentIntent = PendingIntent.getBroadcast(MainActivity.this, 0, new Intent(sent), 0);
            PendingIntent deliveredIntent = PendingIntent.getBroadcast(MainActivity.this, 0, new Intent(delivered), 0);

            /** Below broadcastReceiver deals with SMS sending toast */
            registerReceiver(new BroadcastReceiver() {
                @Override
                public void onReceive(Context context, Intent intent) {
                    switch (getResultCode()) {
                        /** If SMS sent, below code is executed */
                        case Activity.RESULT_OK:
                            Toast.makeText(getBaseContext(), "SMS sent",
                                    Toast.LENGTH_SHORT).show();
                            break;
                        /** If SMS not sent, below code is executed */
                        case Activity.RESULT_CANCELED:
                            Toast.makeText(getBaseContext(), "SMS not sent",
                                    Toast.LENGTH_SHORT).show();
                            break;
                    }
                    /** Unregistering the broadcastReceiver */
                    unregisterReceiver(this);
                }
            }, new IntentFilter(sent));


            /** BroadcastReceiver which deals with SMS delivered toast */
            registerReceiver(new BroadcastReceiver() {
                @Override
                public void onReceive(Context context, Intent intent) {
                    switch (getResultCode()) {
                        /** If SMS delivered, below code is executed */
                        case Activity.RESULT_OK:
                            Toast.makeText(getBaseContext(), "SMS delivered",
                                    Toast.LENGTH_SHORT).show();
                            break;
                        /** If SMS not delivered, below code is executed */
                        case Activity.RESULT_CANCELED:
                            Toast.makeText(getBaseContext(), "SMS not delivered",
                                    Toast.LENGTH_SHORT).show();
                            break;
                    }
                    /** Unregistering the broadcastReceiver */
                    unregisterReceiver(this);
                }
            }, new IntentFilter(delivered));

            if (number.length() > 0) {
                /** Sending the SMS when the phone number is provided */
                smsManager.sendTextMessage(number, null, message, sentIntent, deliveredIntent);
            } else {
                Toast.makeText(getBaseContext(),"Please enter phone number.", Toast.LENGTH_SHORT).show();
            }
        }
    }

    /** Callback for the result from requesting permissions */
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case LOCATION: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    getLocation();
                } else {
                }
                return;
            }

            case SMS: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    sendSMS();

                } else {
                }
                return;
            }
        }
    }
}
