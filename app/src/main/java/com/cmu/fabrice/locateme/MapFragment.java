package com.cmu.fabrice.locateme;

/**
 * This is a Fragment which defines what is going to be displayed on the map
 *
 * @author: Fabrice Kwizera
 * <p>
 * Andrew ID: fkwizera
 * <p>
 * On my honor, as a Carnegie-Mellon Africa student, I have neither given nor received unauthorized assistance on this work.
 */


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

public class MapFragment extends Fragment implements OnMapReadyCallback {

    private double latitude;
    private double longitude;

    public MapFragment() {

    }

    /** Using the Fragment to set the map on the layout */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment, container, false);
        SupportMapFragment supportMapFragment = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.map);
        supportMapFragment.getMapAsync(this);
        return view;
    }

    /** Setters and getters of coordinates to be used the show the location on the map*/
    public void setLatitude(double _latitude) {
        latitude = _latitude;
    }

    public void setLongitude(double _longitude) {
        longitude = _longitude;
    }

    public double getLatitude() {
        return latitude;
    }

    public double getLongitude() {
        return longitude;
    }


    /** Show on a Google map the location using the latitude and longitude provided */
    @Override
    public void onMapReady(GoogleMap _googleMap) {
        GoogleMap googleMap = _googleMap;
        LatLng whereIam = new LatLng(getLatitude(), getLongitude());
        googleMap.addMarker(new MarkerOptions().position(whereIam).title("This device")).showInfoWindow();
        googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(whereIam, 17));
    }
}
